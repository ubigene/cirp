/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitnessfunction;

/**
 *
 * @author e200165
 */
public class Force {

    //Process Parameters

    private double fz = 1;
    private double Vc = 120;
    private double D = 40;
    private int z = 4;                   // Number of teeth?
    private double RPM;
    private boolean Conventional = true; //True=conventional milling False=climb milling

    private double az = 1;
    private double axy;
    private double poss = 0;
    private double time = 0;

    //Material Constants (do not change)
    double F_z = 0.22;
    double F_x = 0.702;
    double F_y = 0.652;
    double Ks = 159.327;
    double Kv = 33.0707;
    double Kr = 32.9;

    public Force() {
        RPM = Vc * 1000 / (Math.PI * D * 60) * 60;
    }
    
    /**
     * Step from the current model time to newTime
     * @param newTime  
     * @param feedOverride
     * @return 
     */
    public double[] step(double newTime, double feedOverride) {
        Update_axy(poss);
        double stepSize = newTime - time;
        time = newTime;
        double[] forces = Fcalc(feedOverride);
        poss += stepSize * 1 * fz * RPM / 60 * z;
        return forces;
    }

    public double[] Fcalc(double feed_over) {
        double RPS = RPM / 60;
        double rots = (time * RPS) % 1;     // Rotation phase (0..1)
        double fzi;
        double toothspacing = 360 / z;
        double sigma;
        double[][] F = new double[z][3];
        double[][] Fxyz = new double[z][3];
        double[] Fcalc = new double[3];
        Fcalc[0] = 0;
        Fcalc[1] = 0;
        Fcalc[2] = 0;
        sigma = Math.toDegrees(Math.acos((D * 0.5 - axy) / (0.5 * D)));
        for (int tooth = 0; tooth < z; tooth++) {
            double theta = rots * 360 + toothspacing * tooth;
            theta = theta % 360;
            if (theta < sigma) {
                fzi = Math.max(Math.sin(Math.toRadians(theta)), 0) * fz * feed_over;
            } else {
                fzi = 0;
            }
            double Fs;
            double Fv;
            double Fr;
            double Fd;
            Fs = az * Ks * Math.pow(fzi, F_z);
            Fr = az * Kr * Math.pow(fzi, F_y);
            Fv = az * Kv * Math.pow(fzi, F_x);
            Fd = Math.sqrt(Fr * Fr + Fs * Fs);

            F[tooth][0] = Fs;
            F[tooth][1] = Fr * Math.cos(Math.PI / 4) - Fv * Math.cos(Math.PI / 4);
            F[tooth][2] = Fv * Math.sin(Math.PI / 4) + Fr * Math.cos(Math.PI / 4);

            Fxyz[tooth][0] = Math.cos(Math.toRadians(theta)) * F[tooth][0] + Math.sin(Math.toRadians(theta)) * F[tooth][1];
            Fxyz[tooth][1] = -(Math.sin(Math.toRadians(theta)) * F[tooth][0] - Math.cos(Math.toRadians(theta)) * F[tooth][1]);
            Fxyz[tooth][2] = F[tooth][2];

            Fcalc[0] += Fxyz[tooth][0];
            Fcalc[1] += Fxyz[tooth][1];
            Fcalc[2] += Fxyz[tooth][2];
        }
        return Fcalc;
    }

    public void Update_axy(double pos) {
        axy = Math.tan(Math.toRadians(10)) * pos;
    }

    /**
     * @return the fz
     */
    public double getFz() {
        return fz;
    }

    /**
     * @param fz the fz to set
     */
    public void setFz(double fz) {
        this.fz = fz;
    }

    /**
     * @return the Vc
     */
    public double getVc() {
        return Vc;
    }

    /**
     * @param Vc the Vc to set
     */
    public void setVc(double Vc) {
        this.Vc = Vc;
    }

    /**
     * @return the D
     */
    public double getD() {
        return D;
    }

    /**
     * @param D the D to set
     */
    public void setD(double D) {
        this.D = D;
    }

    /**
     * @return the az
     */
    public double getAz() {
        return az;
    }

    /**
     * @param az the az to set
     */
    public void setAz(double az) {
        this.az = az;
    }

    /**
     * @return the axy
     */
    public double getAxy() {
        return axy;
    }

    /**
     * @param axy the axy to set
     */
    public void setAxy(double axy) {
        this.axy = axy;
    }

    /**
     * @return the Conventional
     */
    public boolean isConventional() {
        return Conventional;
    }

    /**
     * @param Conventional the Conventional to set
     */
    public void setConventional(boolean Conventional) {
        this.Conventional = Conventional;
    }

    /**
     * @return the z
     */
    public int getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    public void setZ(int z) {
        this.z = z;
    }

    /**
     * @return the RPM
     */
    public double getRPM() {
        return RPM;
    }
}
