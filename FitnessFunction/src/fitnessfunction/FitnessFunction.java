/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitnessfunction;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author e200165
 */
public class FitnessFunction {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final double maxTime = 6.25;
        final double step = 0.00025;              // simulation step
        Force N = new Force();

        try (FileWriter f0 = new FileWriter("output.txt")) {
            String newLine = System.getProperty("line.separator");
            for (int i = 0; i < maxTime / step; i++) {
                double time = i * step;
                double[] forces = N.step(time, 1);
//                System.out.println(time + "  " + forces[0] + " " + forces[1] + " " + forces[2]);
                f0.write(time + "  " + forces[0] + " " + forces[1] + " " + forces[2] + newLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
